/*
 * Copyright (C) 2019-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "theme_image_provider.h"

#include <QDebug>
#include <QIcon>
#include <QStandardPaths>

using namespace Imaginario;

ThemeImageProvider::ThemeImageProvider():
    QQuickImageProvider(QQmlImageProviderBase::Pixmap)
{
    QString localShare =
        QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);

    /* Applications icons can also be found in
     * ~/.local/share/icons/, which is not in the icon theme search
     * path. Let's add it.
     */
    QStringList searchPaths = QIcon::themeSearchPaths();
    searchPaths.append(localShare + "/icons");
    QIcon::setThemeSearchPaths(searchPaths);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 11, 0))
    /* The same directory is used for unthemed icons, so we also
     * add it to the fallback search path.
     */
    searchPaths = QIcon::fallbackSearchPaths();
    searchPaths.append(localShare + "/icons");

    /* Some applications (like ufraw) put their icon in
     * /usr/share/pixmaps; add that directory, too.
     */
    searchPaths.append("/usr/share/pixmaps");

    QIcon::setFallbackSearchPaths(searchPaths);
#endif
}

QPixmap ThemeImageProvider::requestPixmap(const QString &id,
                                          QSize *size,
                                          const QSize &requestedSize)
{
    QString iconId(id);
    int dotPos = id.indexOf('.');
    if (dotPos > 0) {
        iconId = id.left(dotPos);
    }
    QIcon icon = QIcon::fromTheme(iconId);
    if (Q_UNLIKELY(icon.isNull())) {
        return QPixmap();
    }

    QPixmap pixmap =
        icon.pixmap(requestedSize.isValid() ? requestedSize : QSize(32, 32));
    *size = pixmap.size();
    return pixmap;
}
