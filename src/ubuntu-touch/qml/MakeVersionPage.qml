import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property int photoId: -1

    property Item __page: null

    title: i18n.tr("Mark as version")

    Flickable {
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id: column
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
            spacing: units.gu(2)

            ListItem.Expandable {
                id: helpExpandable
                onClicked: expanded = !expanded
                expandedHeight: helpTrigger.height + helpText.height + units.gu(2)
                Label {
                    id: helpTrigger
                    anchors { left: parent.left; right: parent.right }
                    height: helpExpandable.collapsedHeight
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    textFormat: Text.RichText
                    text: i18n.tr("<a href=\"http://localhost/help\">What are versions?</a>")
                }

                Label {
                    id: helpText
                    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                    visible: helpExpandable.expanded
                    wrapMode: Text.Wrap
                    textFormat: Text.RichText
                    text: i18n.tr("<p>You can mark two or more photos as different versions (or edits) of the same shot.<p>Only the final version will be shown in the photos overview. The other versions of the photo will still be visibles in search results, and when tapping on the \"Versions\" icon <img src=\"qrc:/icons/versions\" height=\"%1\"> in the photo viewer page.").arg(units.gu(2))
                }
            }

            Label {
                anchors { left: parent.left; right: parent.right }
                wrapMode: Text.Wrap
                text: i18n.tr("You are about to mark this photo as an edited version of an original photo.")
            }

            ListItem.SingleControl {
                highlightWhenPressed: false
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Choose original photo")
                    iconSource: "qrc:/icons/versions"
                    anchors {
                        fill: parent
                        margins: units.gu(1)
                    }
                    onClicked: __page = pageStack.push(Qt.resolvedUrl("SelectImagePage.qml"), {
                        title: i18n.tr("Pick original"),
                        model: photoModel,
                    })
                }
            }
        }
    }

    Writer {
        id: writer
    }

    PhotoModel {
        id: photoModel
        excludedPhotos: root.photoId
    }

    Connections {
        target: __page
        onItemSelected: {
            root.setOriginalPhoto(photoId)
            pageStack.pop()
            pageStack.pop()
        }
    }

    function setOriginalPhoto(originalId) {
        console.log("Setting " + photoId + " as original for " + originalId)
        if (originalId != photoId) {
            writer.makeVersion(originalId, photoId)
        }
    }
}
