import QtQuick 2.0
import Ubuntu.Components 1.3

Rectangle {
    id: root

    property real zoomLevel

    height: units.gu(2)
    width: units.gu(10)
    border { color: "white"; width: units.dp(1) }
    color: "transparent"

    Rectangle {
        color: "black"
        opacity: 0.3
        radius: units.gu(1)
        anchors { margins: -units.gu(1); left: parent.left; right: parent.right; top: parent.top; bottom: label.bottom }
    }

    Rectangle {
        id: downscale
        anchors { margins: parent.border.width; left: parent.left; top: parent.top; bottom: parent.bottom }
        color: "green"
        width: Math.min(zoomLevel, 1.0) * parent.width / 2
    }

    Rectangle {
        anchors { margins: parent.border.width; top: parent.top; bottom: parent.bottom }
        color: "yellow"
        x: parent.width / 2
        width: Math.max(zoomLevel - 1, 0.0) * parent.width / 8
    }

    Text {
        id: label
        anchors { top: parent.bottom; horizontalCenter: parent.horizontalCenter }
        text: i18n.tr("%1x").arg(zoomLevel.toFixed(1))
        textFormat: Text.PlainText
        color: "white"
    }
}
