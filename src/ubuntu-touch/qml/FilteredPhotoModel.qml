import QtQuick 2.0
import Imaginario 1.0

PhotoModel {
    id: root

    property string filterName: ""
    property bool hasFilter: false

    skipNonDefaultVersions: true

    Binding on forbiddenTags {
        value: Settings.hiddenTags
        when: !hasFilter
    }

    function showRoll(rollId) {
        roll0 = rollId
        roll1 = rollId
        skipNonDefaultVersions = false
        forbiddenTags = []
        hasFilter = true
    }

    function clearFilter() {
        var invalidDate = new Date(NaN)
        var invalidLocation = Utils.geo(null)
        roll0 = -1
        roll1 = -1
        location0 = invalidLocation
        location1 = invalidLocation
        time0 = invalidDate
        time1 = invalidDate
        requiredTags = []
        skipNonDefaultVersions = true
        hasFilter = false
    }
}
