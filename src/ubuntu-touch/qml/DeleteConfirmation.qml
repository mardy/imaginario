import Imaginario 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: root

    property var model
    property var selectedIndexes
    property bool confirmed: false

    signal closed

    title: i18n.tr("File deletion")
    text: i18n.tr("Confirm deletion of the selected file?",
                  "Confirm deletion of the selected files?", selectedIndexes.length)
    onVisibleChanged: if (!visible) closed()

    Writer {
        id: writer
        embed: Settings.embed
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: setConfirmed(false)
    }

    Button {
        text: i18n.tr("Delete file", "Delete files", selectedIndexes.length)
        color: "red"
        onClicked: setConfirmed(true)
    }

    function setConfirmed(isConfirmed) {
        confirmed = isConfirmed
        if (confirmed) {
            var ids = []
            for (var i = 0; i < selectedIndexes.length; i++) {
                var id = model.get(selectedIndexes[i], "photoId")
                ids.push(id)
            }
            writer.deletePhotos(ids)
            model.deletePhotos(selectedIndexes)
        }
        PopupUtils.close(root)
    }
}
