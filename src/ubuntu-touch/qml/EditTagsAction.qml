import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Action {
    property var photoIds

    text: i18n.tr("Edit tags")
    iconSource: "qrc:/icons/edit-tags"
    enabled: photoIds.length > 0
    onTriggered: pageStack.push(Qt.resolvedUrl("TagEditSheetFull.qml"),
        { "photoIds": photoIds })
}
