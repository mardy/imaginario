/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "duplicate_detector.h"

#include "database.h"
#include "metadata.h"

#include <QDebug>
#include <QFileInfo>

using namespace Imaginario;

namespace Imaginario {

class DuplicateDetectorPrivate
{
    Q_DECLARE_PUBLIC(DuplicateDetector)

public:
    DuplicateDetectorPrivate(DuplicateDetector *q);
    ~DuplicateDetectorPrivate();

    double computeScore(const QString &fileName,
                        const Metadata::ImportData &data,
                        const Database::Photo &photo);
    MaybeDuplicates computeScore(const QUrl &url,
                                 const Metadata::ImportData &data,
                                 const QList<Database::Photo> &photos);
    static bool isExtensionRaw(const QString &extension);

private:
    Metadata m_metadata;
    double m_minimumScore;
    QHash<QUrl, MaybeDuplicates> m_duplicates;
    mutable DuplicateDetector *q_ptr;
};

} // namespace

DuplicateDetectorPrivate::DuplicateDetectorPrivate(DuplicateDetector *q):
    m_minimumScore(0.5),
    q_ptr(q)
{
}

DuplicateDetectorPrivate::~DuplicateDetectorPrivate()
{
}

/* This implementation is taken from
 * https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C.2B.2B
 */
static int levenshteinDistance(const QString &s1, const QString &s2)
{
    // To change the type this function manipulates and returns, change
    // the return type and the types of the two variables below.
    int s1len = s1.size();
    int s2len = s2.size();

    auto column_start = (decltype(s1len))1;

    auto column = new decltype(s1len)[s1len + 1];
    std::iota(column + column_start, column + s1len + 1, column_start);

    for (auto x = column_start; x <= s2len; x++) {
        column[0] = x;
        auto last_diagonal = x - column_start;
        for (auto y = column_start; y <= s1len; y++) {
            auto old_diagonal = column[y];
            auto possibilities = {
                column[y] + 1,
                column[y - 1] + 1,
                last_diagonal + (s1[y - 1] == s2[x - 1]? 0 : 1)
            };
            column[y] = std::min(possibilities);
            last_diagonal = old_diagonal;
        }
    }
    auto result = column[s1len];
    delete[] column;
    return result;
}

double DuplicateDetectorPrivate::computeScore(const QString &fileName,
                                              const Metadata::ImportData &data,
                                              const Database::Photo &photo)
{
    double score = 0.0;

    if (data.time == photo.time()) score += 0.4;

    if (!data.title.isEmpty() && data.title == photo.description()) {
        score += 0.4;
    }

    int distance = levenshteinDistance(fileName, photo.fileName().toLower());
    score += qMax(0.0, (15 - distance) / 30.0);

    return qMin(score, 1.0);
}

static bool compareDups(const MaybeDuplicate &d0, const MaybeDuplicate &d1)
{
    return d0.score > d1.score;
}

MaybeDuplicates
DuplicateDetectorPrivate::computeScore(const QUrl &url,
                                       const Metadata::ImportData &data,
                                       const QList<Database::Photo> &photos)
{
    QString fileName = QFileInfo(url.toLocalFile()).fileName().toLower();
    MaybeDuplicates dups;
    Q_FOREACH(const Database::Photo &photo, photos) {
        double score = computeScore(fileName, data, photo);
        if (score >= m_minimumScore) {
            dups.append(MaybeDuplicate(photo.id(), score));
        }
    }
    std::sort(dups.begin(), dups.end(), compareDups);
    return dups;
}

bool DuplicateDetectorPrivate::isExtensionRaw(const QString &extension)
{
    QByteArray ext = extension.toLatin1();
    if (ext == "jpg") return false;

    /* From https://en.wikipedia.org/wiki/Raw_image_format */
    static const char *const rawExtensions[] = {
        "3fr",
        "ari", "arw",
        "bay",
        "crw", "cr2",
        "cap",
        "data", "dcs", "dcr", "dng",
        "drf",
        "eip", "erf",
        "fff",
        "iiq",
        "k25", "kdc",
        "mdc", "mef", "mos", "mrw",
        "nef", "nrw",
        "obm", "orf",
        "pef", "ptx", "pxn",
        "r3d", "raf", "raw", "rwl", "rw2", "rwz",
        "sr2", "srf", "srw",
        "tif",
        "x3f",
        0
    };
    for (const char *const *ptr = rawExtensions; *ptr != 0; ptr++) {
        if (ext == *ptr) return true;
    }
    return false;
}

DuplicateDetector::DuplicateDetector(QObject *parent):
    QObject(parent),
    d_ptr(new DuplicateDetectorPrivate(this))
{
}

DuplicateDetector::~DuplicateDetector()
{
    delete d_ptr;
}

void DuplicateDetector::setMinimumScore(double score)
{
    Q_D(DuplicateDetector);
    d->m_minimumScore = score;
    Q_EMIT minimumScoreChanged();
}

double DuplicateDetector::minimumScore() const
{
    Q_D(const DuplicateDetector);
    return d->m_minimumScore;
}

bool DuplicateDetector::checkFile(const QUrl &url)
{
    Q_D(DuplicateDetector);

    Metadata::ImportData data;
    if (d->m_metadata.readImportData(url.toLocalFile(), data) &&
        data.time.isValid()) {
        SearchFilters filters;
        filters.time0 = filters.time1 = data.time;
        filters.skipNonDefaultVersions = true;
        QList<Database::Photo> photos =
            Database::instance()->findPhotos(filters);
        d->m_duplicates[url] = d->computeScore(url, data, photos);
    }

    return d->m_duplicates.contains(url) && !d->m_duplicates[url].isEmpty();
}

MaybeDuplicates DuplicateDetector::duplicatesFor(const QUrl &url) const
{
    Q_D(const DuplicateDetector);
    return d->m_duplicates[url];
}

DuplicateDetector::RawPairs
DuplicateDetector::makeRawPairs(const QList<QUrl> &urls) const
{
    Q_D(const DuplicateDetector);
    RawPairs pairs;
    QStringList baseNames;
    for (const QUrl &url: urls) {
        QFileInfo fileInfo(url.path());
        QString baseName = fileInfo.baseName().toLower();
        QString extension = fileInfo.suffix().toLower();
        int i = baseNames.indexOf(baseName);
        bool isRaw = d->isExtensionRaw(extension);
        if (i < 0) {
            i = pairs.count();
            pairs.append(RawPair());
            baseNames.append(baseName);
        }

        RawPair &p = pairs[i];
        QUrl &destination = isRaw ? p.raw : p.edited;
        if (destination.isEmpty()) {
            destination = url;
        } else {
            /* The basename similarity is probably coincidental */
            RawPair pair;
            if (isRaw) pair.raw = url;
            else pair.edited = url;
            pairs.append(pair);
            baseNames.append(baseName);
        }
    }
    return pairs;
}
