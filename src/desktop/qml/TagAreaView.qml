import Imaginario 1.0
import QtQuick 2.5

Item {
    id: root

    property alias delegate: repeater.delegate
    property alias photoId: areaModel.photoId
    property alias editingAreas: areaModel.editingAreas

    signal deletionRequested(var tag)

    TagAreaModel {
        id: areaModel
    }

    Repeater {
        id: repeater

        property int highestZ: 0

        model: areaModel
        delegate: TagAreaDelegate {
            tagName: model.tagName
            editing: model.editing
            x: rect.x * parent.width
            y: rect.y * parent.height
            width: rect.width * parent.width
            height: rect.height * parent.height
            maxLabelWidth: root.width
            onEntered: z = ++repeater.highestZ
            onDeletionRequested: root.deletionRequested(tag)
        }
    }
}
