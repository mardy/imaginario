import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0

Dialog {
    id: root

    property alias panelImplicitWidth: tabView.implicitWidth
    property alias panelImplicitHeight: tabView.implicitHeight
    property alias currentIndex: tabView.currentIndex
    default property alias data: tabView.data

    standardButtons: StandardButton.NoButton
    width: tabView.implicitWidth + 24 // workaround for QTBUG-49058
    height: tabView.implicitHeight + 24

    ColumnLayout {
        anchors.fill: parent
        TabView {
            id: tabView

            Layout.fillWidth: true
            Layout.fillHeight: true

            property bool canBack: __history.length > 0
            property bool canNext: useProperty("done", true, false)
            property string nextButtonLabel: usePropertyIfTrue("nextButtonLabel", qsTr("Next"))
            property bool isLast: useProperty("isLast", currentIndex == count - 1)
            property Item currentItem: null

            implicitWidth: 600
            implicitHeight: 400
            frameVisible: false
            tabsVisible: false

            property var __history: []

            onCurrentIndexChanged: updateCurrentItem()
            onCountChanged: updateCurrentItem()

            function useProperty(name, defaultValue, defaultUnloaded) {
                return currentItem ? (currentItem.hasOwnProperty(name) ? currentItem[name] : defaultValue) : defaultUnloaded
            }

            function usePropertyIfTrue(name, defaultValue, defaultUnloaded) {
                var value = useProperty(name, defaultValue, defaultUnloaded)
                return value ? value : defaultValue
            }

            function updateCurrentItem() {
                if (count > 0) {
                    currentItem = getTab(currentIndex)
                } else {
                    currentItem = null
                }
            }

            function jumpTo(index) {
                var t = __history
                __history.push(currentIndex)
                __history = t
                currentIndex = index
            }

            function back() {
                var t = __history
                currentIndex = t.pop()
                __history = t
            }

            function next() {
                var tab = getTab(currentIndex)
                if (tab.hasOwnProperty("onConfirmed")) {
                    tab.onConfirmed()
                } else {
                    if (isLast) {
                        root.close()
                    } else {
                        var i = 1
                        while (!getTab(currentIndex + i).enabled) {
                            i++
                        }

                        jumpTo(currentIndex + i)
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignRight

            Button {
                enabled: tabView.currentIndex == 0
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: root.close()
            }

            Button {
                enabled: tabView.canBack
                text: qsTr("Previous")
                iconName: "back"
                onClicked: tabView.back()
            }

            Button {
                visible: !closeButton.visible
                enabled: tabView.canNext
                text: tabView.nextButtonLabel
                iconName: "next"
                onClicked: tabView.next()
            }

            Button {
                id: closeButton
                visible: tabView.isLast
                text: qsTr("Close")
                iconName: "dialog-ok"
                onClicked: tabView.next()
            }
        }
    }

    function jumpTo(index) { tabView.jumpTo(index) }
}
