.import "popupUtils.js" as PopupUtils

function open(photoModel, urls) {
    var item = PopupUtils.open(Qt.resolvedUrl("ImportDialog.qml"), this, {
        "urls": urls,
    })
    item.done.connect(function(rollId) {
        if (rollId >= 0) {
            photoModel.showRoll(rollId)
        }
    })
}
