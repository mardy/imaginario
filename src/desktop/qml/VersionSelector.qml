import Imaginario 1.0
import QtQuick 2.0
import QtQuick.Controls 1.4
import "."

ComboBox {
    id: root

    property var photoId: -1

    enabled: versionListModel.count > 1
    model: fillModel()
    textRole: "text"
    onActivated: if (!versionModel.get(index, "isDefaultVersion")) {
        var photoId = versionModel.get(index, "photoId")
        GlobalWriter.makeDefaultVersion(photoId)
    }

    ListModel {
        id: versionListModel
    }
    PhotoModel {
        id: versionModel
        property string __original: qsTr("Original")
        onModelReset: {
            versionListModel.clear()
            if (versionModel.count == 0) {
                versionListModel.append({
                    "text": __original,
                })
                root.currentIndex = 0
                return
            }

            var commonPrefix = ""
            var commonSuffix = ""
            for (var i = 0; i < versionModel.count; i++) {
                var fileName = versionModel.get(i, "fileName")
                if (i == 0) {
                    commonPrefix = fileName
                    commonSuffix = fileName
                } else {
                    commonPrefix = root.findCommonPrefix(commonPrefix, fileName)
                    commonSuffix = root.findCommonSuffix(commonSuffix, fileName)
                }
            }

            var suffixStart = commonSuffix.indexOf('.')
            if (suffixStart > 0) {
                commonSuffix = commonSuffix.substring(suffixStart)
            }
            var defaultIndex = 0
            for (var i = 0; i < versionModel.count; i++) {
                var fileName = versionModel.get(i, "fileName")
                if (versionModel.get(i, "isDefaultVersion")) {
                    defaultIndex = i
                }
                versionListModel.append({
                    "text": versionName(fileName, commonPrefix, commonSuffix)
                })
            }
            root.currentIndex = defaultIndex
        }

        function versionName(fileName, prefix, suffix) {
            var name = fileName.substring(prefix.length, fileName.length - suffix.length)
            return name ? name : __original
        }
    }

    function findCommonPrefix(a1, a2) {
        var l = a1.length
        var i = 0
        while (i < l && a1.charAt(i) === a2.charAt(i)) i++;
        return a1.substring(0, i)
    }

    function findCommonSuffix(a1, a2) {
        var l1 = a1.length
        var l2 = a2.length
        var i = 0
        while (i < l1 && i < l2 && a1.charAt(l1 - i - 1) === a2.charAt(l2 - i - 1)) i++;
        return a1.substring(l1 - i)
    }

    function fillModel() {
        versionListModel.clear()
        root.currentIndex = -1
        versionModel.photoVersions = root.photoId
        return versionListModel
    }
}
