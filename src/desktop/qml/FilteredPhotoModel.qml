import QtQuick 2.0
import Imaginario 1.0

PhotoModel {
    id: root

    property string filterName: ""
    property bool hasFilter: false
    property bool hasRatingFilter: rating0 != -1 || rating1 != -1
    property bool hasRollFilter: roll0 != -1 || roll1 != -1
    property bool hasTimeFilter: !isNaN(time0) || !isNaN(time1)
    property int updateCounter: 0

    skipNonDefaultVersions: true
    onModelReset: updateCounter++

    property var __invalidDate: new Date(NaN)

    Binding on forbiddenTags {
        value: Settings.hiddenTags
        when: !hasFilter
    }

    function showLastImportRoll(rollModel) {
        showRoll(rollModel.lastRollId)
    }

    function showRoll(rollId) {
        roll0 = rollId
        roll1 = rollId
        skipNonDefaultVersions = false
        forbiddenTags = []
        hasFilter = true
    }

    function clearFilter() {
        var invalidLocation = Utils.geo(null)
        roll0 = -1
        roll1 = -1
        rating0 = -1
        rating1 = -1
        location0 = invalidLocation
        location1 = invalidLocation
        nonGeoTagged = false
        time0 = __invalidDate
        time1 = __invalidDate
        requiredTags = []
        areaTagged = false
        skipNonDefaultVersions = true
        hasFilter = false
    }

    function clearRatingFilter() {
        rating0 = -1
        rating1 = -1
    }

    function clearRollFilter() {
        roll0 = -1
        roll1 = -1
    }

    function clearTimeFilter() {
        time0 = __invalidDate
        time1 = __invalidDate
    }
}
