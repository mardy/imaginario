import QtQuick 2.0

Row {
    id: root

    property alias model: repeater.model

    height: 16
    spacing: 2

    Repeater {
        id: repeater
        delegate: Image {
            width: 16
            height: 16
            sourceSize { width: width; height: height }
            source: model.iconSource
        }
    }
}
