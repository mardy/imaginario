import QtQuick 2.5
import QtQuick.Window 2.2

MouseArea {
    id: root

    property string tagName: ""
    property bool editing: false
    property int maxLabelWidth: 100 * Screen.devicePixelRatio

    signal deletionRequested()

    hoverEnabled: true
    acceptedButtons: Qt.NoButton

    Rectangle {
        id: areaBorder
        anchors.fill: parent
        border { color: root.editing ? "yellow" : "green"; width: 2 }
        color: "transparent"
        opacity: root.containsMouse ? 1.0 : 0.8
    }

    Rectangle {
        id: tagBubble
        anchors { horizontalCenter: parent.horizontalCenter; top: parent.bottom; margins: 2 }
        border { color: "black"; width: 2 }
        width: Math.min(label.implicitWidth, root.maxLabelWidth) + radius * 2
        height: label.implicitHeight + 4
        color: "white"
        radius: height / 2
        visible: label.text.length > 0
        opacity: 0.8

        Text {
            id: label
            anchors {
                left: parent.left; leftMargin: parent.radius
                right: parent.right; rightMargin: parent.radius
                verticalCenter: parent.verticalCenter
            }
            text: tagName
            font.pointSize: 8
            elide: Text.ElideMiddle
        }
    }

    Image {
        anchors { horizontalCenter: areaBorder.right; verticalCenter: areaBorder.top }
        width: 20
        height: width
        visible: root.containsMouse || closeArea.containsMouse
        source: "qrc:/icons/remove-tag"
        sourceSize.width: width

        MouseArea {
            id: closeArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: root.deletionRequested()
        }
    }
}
