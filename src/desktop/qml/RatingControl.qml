import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    id: root

    property int rating: 0
    property bool editable: true

    signal ratingSet(int rating)

    property int _starSize: starRow.height
    property int _hoverRating: mouseArea.containsMouse ?
        (1 + (mouseArea.mouseX - starRow.anchors.leftMargin) / _starSize) : rating

    onRatingSet: root.rating = rating

    Row {
        id: starRow
        anchors {
            left: parent.left; leftMargin: 10
            top: parent.top; bottom: parent.bottom
        }
        Repeater {
            model: 5
            Image {
                anchors { top: parent.top; bottom: parent.bottom }
                width: height
                sourceSize { width: width; height: height }
                source: "qrc:/icons/rating_" + (index < _hoverRating ? "gold" : (index < rating ? "black" : "empty"))
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors { left: parent.left; top: starRow.top; bottom: starRow.bottom; right: starRow.right }
        enabled: editable
        hoverEnabled: true
        onClicked: root.ratingSet(Math.min(_hoverRating, 5))
    }
}
