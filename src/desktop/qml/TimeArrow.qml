import Imaginario 1.0
import QtQuick 2.0

Image {
    id: root

    property Item timeBar: null
    property int center: width / 2
    property int restPosition: 0
    property int requestedPosition: restPosition
    property date followedTime
    property bool older: false

    signal monthSelected(var date)

    property var __invalidDate: new Date(NaN)

    x: requestedPosition - center
    source: "qrc:/icons/time_arrow"
    sourceSize.width: width
    fillMode: Image.PreserveAspectCrop
    smooth: true
    rotation: older ? 180 : 0

    Drag.hotSpot: Qt.point(width / 2, 0)

    Timer {
        id: visibilityTimer
        interval: 100
        repeat: true
        onTriggered: {
            var margin = 60
            var offset = timeBar.mapFromItem(root, center, 0).x
            timeBar.ensureVisible(offset - margin + timeBar.contentOffset, margin * 2 + width)
        }
    }

    states: [
        State {
            name: "dragging"
            when: mouseArea.drag.active
            PropertyChanges { target: visibilityTimer; running: true }
        },
        State {
            name: "off"
            when: isNaN(followedTime)
            PropertyChanges { target: root; requestedPosition: restPosition }
        },
        State {
            name: "on"
            when: !isNaN(followedTime)
            PropertyChanges { target: root; requestedPosition: posFromTime(followedTime) }
            PropertyChanges { target: root; x: requestedPosition - center - timeBar.contentOffset }
        }
    ]

    MouseArea {
        id: mouseArea
        anchors { left: parent.left; right: parent.right; margins: -1 }
        height: width
        drag.target: parent
        drag.axis: Drag.XAxis
        drag.minimumX: -parent.width / 2
        drag.maximumX: root.parent.width - parent.width / 2
        drag.onActiveChanged: if (!drag.active) {
            var offset = timeBar.mapFromItem(root, center, 0).x
            if (offset < 0 || offset >= timeBar.width) {
                monthSelected(__invalidDate)
            } else {
                var month = timeBar.dateForOffset(offset, TimeWidget.ApproxToCloser)
                if (!root.older) {
                    // pick the last day of the previous month
                    month.setDate(0)
                }
                monthSelected(month)
            }
        }
    }

    function posFromTime(time) {
        var pos = timeBar.offsetForDate(time, root.older ? TimeWidget.ApproxToOlder : TimeWidget.ApproxToNewer)
        return timeBar.mapToItem(parent, pos, 0).x
    }
}
