import Imaginario 1.0
import QtQuick 2.0
import QtQuick.Window 2.2

Window {
    id: root

    property alias initialIndex: display.initialIndex
    property alias currentIndex: display.currentIndex
    property alias model: display.model

    color: "black"
    visible: true
    visibility: Window.FullScreen
    Component.onCompleted: display.show()

    ImageDisplay {
        id: display
        onDone: root.close()
        Keys.onEscapePressed: root.close()
    }
}
