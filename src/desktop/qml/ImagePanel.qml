import Imaginario 1.0
import QtQml 2.2
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import "popupUtils.js" as PopupUtils
import "."

ColumnLayout {
    id: root

    property alias model: imageView.model
    property var selectedTag: null
    property alias selectedIndexes: imageView.selectedIndexes
    property alias tagAreaMode: imageView.tagAreaMode

    Window.onActiveFocusItemChanged: {
        console.log("Active focus item: " + Window.activeFocusItem)
        connections.target = Window.activeFocusItem
    }

    Connections {
        id: connections
        onVisibleChanged: if (!target.visible) imageView.forceActiveFocus()
    }

    TimeBar {
        id: timeWidget
        Layout.fillWidth: true
        height: 30
        photoModel: imageView.model
        // updateCounter forces binding updates
        visibleDate: imageView.model.updateCounter, imageView.model.get(imageView.firstVisibleIndex, "time")
        date0: photoModel.time0
        date1: photoModel.time1

        onDateClicked: {
            /* find the last day of the month */
            date.setMonth(date.getMonth() + 1)
            date.setDate(0)
            var i = imageView.model.firstIndex(PhotoModel.OlderThan, date)
            imageView.positionViewAtIndex(i, GridView.Beginning)
        }
    }

    FilterBar {
        id: filterBar
        Layout.fillWidth: true
        photoModel: imageView.model
        z: 1
    }

    ScrolledImageView {
        id: imageView
        Layout.fillHeight: true
        Layout.fillWidth: true
        selectMode: true
        onItemActivated: root.showFullscreen()
        onRightClicked: contextMenu.popup()

        Connections {
            target: GlobalWriter
            onThumbnailUpdated: {
                var i = model.indexOfItem(photoId)
                if (i >= 0) {
                    model.forceItemRefresh(i);
                }
            }
        }

        Menu {
            id: contextMenu
            property var selectedIndexes: imageView.selectedIndexes

            MenuItem {
                action: actions.copy
            }
            MenuItem {
                text: qsTr("Detach version")
                visible: selectedIndexes.length == 1 && model.get(selectedIndexes[0], "hasVersions")
                onTriggered: GlobalWriter.removeVersion(model.get(selectedIndexes[0], "photoId"))
            }
            MenuSeparator {}
            Instantiator {
                model: HelperModel {
                    id: helperModel
                    helpersData: Settings.helpers
                    mimeType: selectedIndexes.length > 0 ? imageView.model.get(selectedIndexes[0], "mimeType") : Utils.invalidMimeType()
                }
                MenuItem {
                    text: model.actionName
                    enabled: model.canBatch ? (selectedIndexes.length > 0) : (selectedIndexes.length == 1)
                    onTriggered: {
                        var urls = []
                        var masterIds = []
                        var photoIds = []
                        for (var i = 0; i < selectedIndexes.length; i++) {
                            var photoIndex = selectedIndexes[i]
                            masterIds.push(imageView.model.get(photoIndex, "masterVersion"))
                            photoIds.push(imageView.model.get(photoIndex, "photoId"))
                            urls.push(imageView.model.get(photoIndex, "url"))
                        }

                        helperModel.runHelper(index, urls, function (newNames, makeVersion) {
                            if (makeVersion) {
                                console.log("making new version")
                                for (var i = 0; i < newNames.length; i++) {
                                    var newName = newNames[i]
                                    var newId = GlobalWriter.createItem(Qt.resolvedUrl(newName))
                                    if (newId > 0) {
                                        GlobalWriter.makeVersion(masterIds[i], newId)
                                    }
                                }
                            } else if (!model.readOnly) {
                                console.log("Refreshing thumbnails")
                                GlobalWriter.rebuildThumbnails(photoIds)
                            }
                        })
                    }
                }
                onObjectAdded: contextMenu.insertItem(index, object)
                onObjectRemoved: contextMenu.removeItem(object)
            }
            MenuSeparator {}
            MenuItem { action: actions.deleteFromCatalog }
            MenuItem { action: actions.deleteFromDrive }
            MenuSeparator {}
            Menu {
                id: ratm
                title: qsTr("Set rating")
                enabled: selectedIndexes.length > 0
                MenuItem { text: "☆☆☆☆☆"; onTriggered: ratm.setRating(0) }
                MenuItem { text: "★☆☆☆☆"; onTriggered: ratm.setRating(1) }
                MenuItem { text: "★★☆☆☆"; onTriggered: ratm.setRating(2) }
                MenuItem { text: "★★★☆☆"; onTriggered: ratm.setRating(3) }
                MenuItem { text: "★★★★☆"; onTriggered: ratm.setRating(4) }
                MenuItem { text: "★★★★★"; onTriggered: ratm.setRating(5) }

                function setRating(rating) {
                    console.log("Setting rating to " + rating)
                    var photoIds = []
                    var length = selectedIndexes.length
                    for (var i = 0; i < length; i++) {
                        photoIds.push(model.get(selectedIndexes[i], "photoId"))
                    }
                    GlobalWriter.setRating(photoIds, rating)
                }
            }
        }
    }

    TagInputBar {
        id: tagInputRow
        Layout.fillWidth: true
        selectedIndexes: imageView.selectedIndexes
        model: imageView.model
        parentTag: root.selectedTag
        enabled: !areaTagInputBar.visible
    }

    AreaTagInputBar {
        id: areaTagInputBar
        Layout.fillWidth: true
        visible: imageView.tagAreaMode && imageView.tagAreaEditor.hasAreas
        parentTag: root.selectedTag
        onVisibleChanged: if (visible) forceActiveFocus()
        onTagChosen: imageView.tagAreaEditor.assignTag(tag)
        onEditAborted: imageView.tagAreaEditor.clear()
    }

    function handleDropOnTag(tag) {
        var photoIds = []
        var length = selectedIndexes.length
        for (var i = 0; i < length; i++) {
            photoIds.push(model.get(selectedIndexes[i], "photoId"))
        }
        GlobalWriter.changePhotoTags(photoIds, [tag], [])
    }

    function selectAll() { imageView.selectAll() }
    function selectNone() { imageView.selectNone() }

    function getSelectedIds() { return imageView.getSelectedIds() }
    function selectedUrls() { return imageView.selectedUrls() }

    function showFullscreen() {
        PopupUtils.open(Qt.resolvedUrl("DisplayWindow.qml"), this, {
            "initialIndex": imageView.currentIndex,
            "model": imageView.model,
        })
    }
    function zoomIn() { imageView.zoomIn() }
    function zoomOut() { imageView.zoomOut() }

    function toggleTagAreaMode() {
        imageView.tagAreaMode = !imageView.tagAreaMode
    }

    function showTagInputBar() { tagInputRow.show() }
}
