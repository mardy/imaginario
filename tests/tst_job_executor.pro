TARGET = tst_job_executor

include(tests.pri)

QT += \
    concurrent

SOURCES += \
    $${SRC_DIR}/job_executor.cpp \
    tst_job_executor.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/job_database.h \
    $${SRC_DIR}/job_executor.h \
    $${SRC_DIR}/metadata.h \
    test_utils.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
