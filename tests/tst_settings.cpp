/*
 * Copyright (C) 2015-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include <QJsonObject>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class SettingsTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testProperties();
    void testPropertiesCombined_data();
    void testPropertiesCombined();
};

void SettingsTest::testProperties()
{
    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    Settings settings;

    QCOMPARE(settings.writeXmp(), false);
    settings.setWriteXmp(true);
    QCOMPARE(settings.writeXmp(), true);
}

void SettingsTest::testPropertiesCombined_data()
{
    QTest::addColumn<bool>("embed");
    QTest::addColumn<bool>("copyFiles");
    QTest::addColumn<QString>("photoDir");
    QTest::addColumn<QStringList>("hiddenTags");
    QTest::addColumn<QString>("autoTagData");
    QTest::addColumn<bool>("showTags");
    QTest::addColumn<bool>("showAreaTags");
    QTest::addColumn<bool>("showRating");
    QTest::addColumn<QJsonArray>("helpers");

    QTest::newRow("false and nulls") <<
        false << false << QString() << QStringList() <<
        QString() << false << false << false <<
        QJsonArray();

    QTest::newRow("trues and valid") <<
        true << true << "/tmp/foo" <<
        (QStringList() << "nsfw") <<
        "{\"app\": {} }" << true << true << true <<
        QJsonArray{ { true, false }};

    QTest::newRow("mixed") <<
        false << true << "/tmp/bar" <<
        (QStringList() << "one" << "two") <<
        QString() << true << false << true <<
        QJsonArray{};

    QTest::newRow("mixed 2") <<
        true << false << "" <<
        (QStringList() << "nsfw") <<
        "{}" << false << false << true <<
        QJsonArray{ QJsonObject{ {"nice", true }}};

    QTest::newRow("mixed 3") <<
        true << false << "/home/dir/bar" << QStringList() <<
        QString() << false << true << false <<
        QJsonArray{ QJsonObject{ {"one", 1 }}};
}

void SettingsTest::testPropertiesCombined()
{
    QFETCH(bool, embed);
    QFETCH(bool, copyFiles);
    QFETCH(QString, photoDir);
    QFETCH(QStringList, hiddenTags);
    QFETCH(QString, autoTagData);
    QFETCH(bool, showTags);
    QFETCH(bool, showAreaTags);
    QFETCH(bool, showRating);
    QFETCH(QJsonArray, helpers);

    QTemporaryDir tmpDir;
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    Settings settings;
    settings.setEmbed(embed);
    settings.setCopyFiles(copyFiles);
    settings.setPhotoDir(photoDir);
    settings.setHiddentags(hiddenTags);
    settings.setAutoTagData(autoTagData);
    settings.setShowTags(showTags);
    settings.setShowAreaTags(showAreaTags);
    settings.setShowRating(showRating);
    settings.setHelpers(helpers);

    QCOMPARE(settings.embed(), embed);
    QCOMPARE(settings.copyFiles(), copyFiles);
    QCOMPARE(settings.photoDir(), photoDir);
    QCOMPARE(settings.hiddenTags(), hiddenTags);
    QCOMPARE(settings.autoTagData(), autoTagData);
    QCOMPARE(settings.showTags(), showTags);
    QCOMPARE(settings.showAreaTags(), showAreaTags);
    QCOMPARE(settings.showRating(), showRating);
    QCOMPARE(settings.helpers(), helpers);
}

QTEST_GUILESS_MAIN(SettingsTest)

#include "tst_settings.moc"
