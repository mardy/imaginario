TARGET = tst_writer

include(tests.pri)

QT += \
    concurrent \
    qml \

SOURCES += \
    $${SRC_DIR}/utils.cpp \
    $${SRC_DIR}/writer.cpp \
    tst_writer.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/job_executor.h \
    $${SRC_DIR}/utils.h \
    $${SRC_DIR}/writer.h \
    $${SRC_DIR}/metadata.h \
    test_utils.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
