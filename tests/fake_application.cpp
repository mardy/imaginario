/*
 * Copyright (C) 2019-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_application.h"

using namespace Imaginario;

static QString m_dataPath;

void ApplicationPrivate::setDataPath(const QString &dataPath)
{
    m_dataPath = dataPath;
}

Application::Application(int &argc, char **argv):
#ifdef DESKTOP_BUILD
    QApplication(argc, argv),
#else
    QGuiApplication(argc, argv),
#endif
    d_ptr(nullptr)
{
}

Application::~Application()
{
}

QString Application::dataPath() const
{
    // Note that this is called on an invalid object! Don't use "this" here.
    return m_dataPath;
}
