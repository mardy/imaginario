PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE meta (
	id	INTEGER PRIMARY KEY NOT NULL, 
	name	TEXT UNIQUE NOT NULL, 
	data	TEXT
);
INSERT INTO "meta" VALUES(1,'F-Spot Version','0.8.2');
INSERT INTO "meta" VALUES(2,'F-Spot Database Version','18');
INSERT INTO "meta" VALUES(3,'Hidden Tag Id','2');
CREATE TABLE tags (
	id		INTEGER PRIMARY KEY NOT NULL, 
	name		TEXT UNIQUE, 
	category_id	INTEGER, 
	is_category	BOOLEAN, 
	sort_priority	INTEGER, 
	icon		TEXT
);
INSERT INTO "tags" VALUES(1,'Favorites',0,1,-10,'stock_icon:emblem-favorite');
INSERT INTO "tags" VALUES(2,'Hidden',0,0,-9,'stock_icon:emblem-readonly');
INSERT INTO "tags" VALUES(3,'People',0,1,-8,'stock_icon:emblem-people');
INSERT INTO "tags" VALUES(4,'Places',0,1,-8,'stock_icon:emblem-places');
INSERT INTO "tags" VALUES(5,'Events',0,1,-7,'stock_icon:emblem-event');
CREATE TABLE rolls (
	id	INTEGER PRIMARY KEY NOT NULL, 
       time	INTEGER NOT NULL
);
CREATE TABLE exports (
	id			INTEGER PRIMARY KEY NOT NULL, 
	image_id		INTEGER NOT NULL, 
	image_version_id	INTEGER NOT NULL, 
	export_type		TEXT NOT NULL, 
	export_token		TEXT NOT NULL
);
CREATE TABLE jobs (
	id		INTEGER PRIMARY KEY NOT NULL, 
	job_type	TEXT NOT NULL, 
	job_options	TEXT NOT NULL, 
	run_at		INTEGER, 
	job_priority	INTEGER NOT NULL
);
CREATE TABLE photos (
	id			INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
	time			INTEGER NOT NULL, 
	base_uri		STRING NOT NULL, 
	filename		STRING NOT NULL, 
	description		TEXT NOT NULL, 
	roll_id			INTEGER NOT NULL, 
	default_version_id	INTEGER NOT NULL, 
	rating			INTEGER NULL 
);
CREATE TABLE photo_tags (
	photo_id	INTEGER, 
       tag_id		INTEGER, 
       UNIQUE (photo_id, tag_id)
);
CREATE TABLE photo_versions (
	photo_id	INTEGER, 
	version_id	INTEGER, 
	name		STRING, 
	base_uri		STRING NOT NULL, 
	filename		STRING NOT NULL, 
	import_md5		TEXT NULL, 
	protected	BOOLEAN, 
	UNIQUE (photo_id, version_id)
);
CREATE INDEX idx_photo_versions_id ON photo_versions(photo_id);
CREATE INDEX idx_photo_versions_import_md5 ON photo_versions(import_md5);
CREATE INDEX idx_photos_roll_id ON photos(roll_id);
COMMIT;
