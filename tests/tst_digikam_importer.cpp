/*
 * Copyright (C) 2019-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.h"
#include "digikam_importer.h"
#include "test_utils.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QFile>
#include <QProcess>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTest>

using namespace Imaginario;

class DigikamImporterTest: public QObject
{
    Q_OBJECT

public:
    DigikamImporterTest() {
        qRegisterMetaType<PhotoId>("PhotoId");
        qRegisterMetaType<Tag>("Tag");
    }

    void setupSource(const QString &sqlFile, const QString &dirName);

private Q_SLOTS:
    void init();
    void cleanup();
    void testOpen_data();
    void testOpen();
    void testEmpty();
    void testComplete();

private:
    QTemporaryDir *m_tmp;
};

void DigikamImporterTest::setupSource(const QString &sqlFile,
                                      const QString &dirName)
{
    QDir dir(dirName);
    dir.mkpath(".");

    QStringList args;
    args << dir.absoluteFilePath("digikam4.db");
    QProcess sqlite3;
    sqlite3.setStandardInputFile(QString(DATA_DIR) + "/digikam_import/" +
                                 sqlFile);
    sqlite3.start("sqlite3", args);
    QVERIFY(sqlite3.waitForStarted());
    QVERIFY(sqlite3.waitForFinished());
}

void DigikamImporterTest::init()
{
    m_tmp = new QTemporaryDir();
    QVERIFY(m_tmp->isValid());
}

void DigikamImporterTest::cleanup()
{
    if (QTest::currentTestFailed()) {
        m_tmp->setAutoRemove(false);
        qDebug() << "Temporary files have been left in" << m_tmp->path();
    }

    delete m_tmp;
    m_tmp = nullptr;
}

void DigikamImporterTest::testOpen_data()
{
    QTest::addColumn<QString>("dbFile");
    QTest::addColumn<QString>("configFilePath");
    QTest::addColumn<QString>("configFileContents");
    QTest::addColumn<int>("expectedCount");

    QTest::newRow("config not found") <<
        "1.sql" <<
        "not-found-for-sure" <<
        QString() <<
        -1;

    QTest::newRow("empty") <<
        "empty.sql" <<
        ".config/digikamrc" <<
        "[Database Settings]\n"
        "Database Name=DBPATH\n"
        "[General Settings]\n"
        "Version=7\n" <<
        0;

    QTest::newRow("1") <<
        "1.sql" <<
        "home/.kde/share/config/digikamrc" <<
        "[Database Settings]\n"
        "Database Name=DBPATH\n"
        "[General Settings]\n"
        "Version=7\n" <<
        44;
}

void DigikamImporterTest::testOpen()
{
    QFETCH(QString, dbFile);
    QFETCH(QString, configFilePath);
    QFETCH(QString, configFileContents);
    QFETCH(int, expectedCount);

    qputenv("XDG_CONFIG_HOME", m_tmp->path().toUtf8() + "/.config");
    qputenv("XDG_DATA_HOME", m_tmp->path().toUtf8() + "/.local/share");
    qputenv("HOME", m_tmp->path().toUtf8() + "/home");

    QString dbDir(m_tmp->path() + "/Photos/");
    configFileContents.replace("DBPATH", dbDir);
    setupSource(dbFile, dbDir);

    QFileInfo fileInfo(m_tmp->path() + "/" + configFilePath);
    fileInfo.dir().mkpath(".");
    {
        QFile configFile(fileInfo.filePath());
        QVERIFY(configFile.open(QIODevice::WriteOnly | QIODevice::Text));
        configFile.write(configFileContents.toUtf8());
    }

    DigikamImporter importer;
    QCOMPARE(importer.count(), expectedCount);
}

void DigikamImporterTest::testEmpty()
{
    qputenv("XDG_CACHE_HOME", m_tmp->path().toUtf8() + "/.cache");
    qputenv("XDG_CONFIG_HOME", m_tmp->path().toUtf8() + "/.config");
    qputenv("XDG_DATA_HOME", m_tmp->path().toUtf8() + "/.local/share");
    qputenv("HOME", m_tmp->path().toUtf8() + "/home");

    QString configFileContents =
        QStringLiteral("[Database Settings]\n"
                       "Database Name=DBPATH\n"
                       "[General Settings]\n"
                       "Version=4.12.0\n");
    QString dbDir(m_tmp->path() + "/Photos/");
    configFileContents.replace("DBPATH", dbDir);
    setupSource("empty.sql", dbDir);

    QFileInfo fileInfo(m_tmp->path() + "/.config/digikamrc");
    fileInfo.dir().mkpath(".");
    {
        QFile configFile(fileInfo.filePath());
        QVERIFY(configFile.open(QIODevice::WriteOnly | QIODevice::Text));
        configFile.write(configFileContents.toUtf8());
    }

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    DigikamImporter importer;
    QSignalSpy statusChanged(&importer,
                             SIGNAL(statusChanged(DigikamImporter::Status)));

    QTRY_COMPARE(importer.status(), DigikamImporter::Ready);
    QCOMPARE(importer.dbVersion(), QString("7"));
    QCOMPARE(importer.version(), QString("4.12.0"));
    QCOMPARE(importer.count(), 0);

    statusChanged.clear();
    importer.exec();
    QTRY_VERIFY(statusChanged.count() > 0);
    QCOMPARE(statusChanged.at(0).at(0).toInt(),
             int(DigikamImporter::ImportingTags));

    QTRY_VERIFY(statusChanged.count() > 1);
    QCOMPARE(statusChanged.at(1).at(0).toInt(),
             int(DigikamImporter::ImportingRolls));

    QTRY_VERIFY(statusChanged.count() > 2);
    QCOMPARE(statusChanged.at(2).at(0).toInt(),
             int(DigikamImporter::ImportingPhotos));

    QTRY_VERIFY(statusChanged.count() > 3);
    QCOMPARE(statusChanged.at(3).at(0).toInt(),
             int(DigikamImporter::Done));
}

void DigikamImporterTest::testComplete()
{
    qputenv("XDG_CACHE_HOME", m_tmp->path().toUtf8() + "/.cache");
    qputenv("XDG_CONFIG_HOME", m_tmp->path().toUtf8() + "/.config");
    qputenv("XDG_DATA_HOME", m_tmp->path().toUtf8() + "/.local/share");
    qputenv("HOME", m_tmp->path().toUtf8() + "/home");

    QString configFileContents =
        QStringLiteral("[Database Settings]\n"
                       "Database Name=DBPATH\n"
                       "[General Settings]\n"
                       "Version=4.12.0\n");
    QString dbDir(m_tmp->path() + "/Photos/");
    configFileContents.replace("DBPATH", dbDir);
    setupSource("1.sql", dbDir);

    QFileInfo fileInfo(m_tmp->path() + "/.config/digikamrc");
    fileInfo.dir().mkpath(".");
    {
        QFile configFile(fileInfo.filePath());
        QVERIFY(configFile.open(QIODevice::WriteOnly | QIODevice::Text));
        configFile.write(configFileContents.toUtf8());
    }

    QScopedPointer<Database> db(Database::instance());
    QVERIFY(db);

    DigikamImporter importer;
    QSignalSpy statusChanged(&importer,
                             SIGNAL(statusChanged(DigikamImporter::Status)));
    QSignalSpy progressChanged(&importer, SIGNAL(progressChanged()));

    QTRY_COMPARE(importer.status(), DigikamImporter::Ready);
    QCOMPARE(importer.dbVersion(), QString("7"));
    QCOMPARE(importer.version(), QString("4.12.0"));
    QCOMPARE(importer.count(), 44);

    statusChanged.clear();
    importer.exec();
    QTRY_VERIFY(statusChanged.count() > 0);
    QCOMPARE(statusChanged.at(0).at(0).toInt(),
             int(DigikamImporter::ImportingTags));

    QTRY_VERIFY(statusChanged.count() > 1);
    QCOMPARE(statusChanged.at(1).at(0).toInt(),
             int(DigikamImporter::ImportingRolls));

    QTRY_VERIFY(statusChanged.count() > 2);
    QCOMPARE(statusChanged.at(2).at(0).toInt(),
             int(DigikamImporter::ImportingPhotos));

    QTRY_VERIFY(statusChanged.count() > 3);
    QCOMPARE(statusChanged.at(3).at(0).toInt(),
             int(DigikamImporter::Done));

    /* Check tags */

    struct TagData {
        QString name;
        QString parent;
        bool isCategory;
    };
    QVector<TagData> tags {
        { "macro", "", false },
        { "2015-08-23 Macro", "", false },
        { "tarassaco", "fiori", false },
        { "fiori", "", true },
    };
    for (const TagData &td: tags) {
        Tag tag = db->tag(td.name);
        QVERIFY(tag.isValid());
        QString parentName =
            tag.parent().isValid() ? tag.parent().name() : "";
        QCOMPARE(parentName, td.parent);
        QCOMPARE(tag.isCategory(), td.isCategory);
    }

    /* Check rolls */

    QList<Roll> rolls = db->rolls(40);
    QCOMPARE(rolls.count(), 18);
    Roll roll = rolls[0];
    QVERIFY(roll.isValid());
    QCOMPARE(roll.time(),
             QDateTime(QDate::fromString("2019-01-03", Qt::ISODate)));
    roll = rolls[3];
    QVERIFY(roll.isValid());
    QCOMPARE(roll.time(),
             QDateTime(QDate::fromString("2016-04-25", Qt::ISODate)));

    /* Check photos */

    SearchFilters filters;
    QList<Database::Photo> photos = db->findPhotos(filters);
    QCOMPARE(photos.count(), 44);

    /* Check photo tags */

    QHash<QString,QSet<QString>> expectedTags {
        { "DSC00448 (1) (Modified (2)).jpg", { "wood","forest" }},
        { "DSC00461 (1).jpg", { "sunset" }},
        { "DSC02582.jpg", { "church" }},
        { "DSC04981 (1).jpg", { "2015-08-23 Macro","prato","tramonto", "basso profilo","controluce","SAL-100M28" }},
        { "DSC04981 (2).jpg", { "2015-08-23 Macro","prato","tramonto", "basso profilo","controluce","SAL-100M28" }},
        { "DSC04981.jpg", { "2015-08-23 Macro","prato","tramonto", "basso profilo","controluce","SAL-100M28" }},
        { "DSC04967.jpg", { "SAL-100M28","tarassaco","2015-08-23 Macro","fiori","macro" }},
        { "DSC04967 (1).jpg", { "SAL-100M28","tarassaco","2015-08-23 Macro","fiori","macro" }},

    };
    QHash<QString,QSet<QString>> photoTags;
    Q_FOREACH(const auto &p, photos) {
        QList<Tag> tags = db->tags(p.id());
        if (tags.isEmpty()) continue;
        photoTags.insert(p.fileName(), Tag::tagNames(tags).toSet());
    }
    QCOMPARE(photoTags, expectedTags);
}

QTEST_GUILESS_MAIN(DigikamImporterTest)

#include "tst_digikam_importer.moc"
