TARGET = tst_importer

include(tests.pri)

QT += \
    concurrent \
    qml \

SOURCES += \
    $${SRC_DIR}/importer.cpp \
    $${SRC_DIR}/utils.cpp \
    tst_importer.cpp

HEADERS += \
    $${SRC_DIR}/abstract_database.h \
    $${SRC_DIR}/database.h \
    $${SRC_DIR}/duplicate_detector.h \
    $${SRC_DIR}/importer.h \
    $${SRC_DIR}/metadata.h \
    $${SRC_DIR}/thumbnailer.h \
    $${SRC_DIR}/utils.h \
    test_utils.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
