/*
 * Copyright (C) 2016-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Imaginario.
 *
 * Imaginario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Imaginario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_database.h"
#include "photo_model.h"
#include "time_model.h"
#include "test_utils.h"
#include "types.h"

#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QVariantMap>
#include <QVector>

using namespace Imaginario;

typedef QMap<QString,int> MonthCount;

class TimeModelTest: public QObject
{
    Q_OBJECT

public:
    static QVariant get(const QAbstractListModel *model, int row,
                        QString roleName);

private Q_SLOTS:
    void testProperties();
    void testCount_data();
    void testCount();
    void testNormalizedCount_data();
    void testNormalizedCount();
};

QVariant TimeModelTest::get(const QAbstractListModel *model, int row,
                            QString roleName)
{
    QHash<int, QByteArray> roleNames = model->roleNames();

    int role = roleNames.key(roleName.toLatin1(), -1);
    return model->data(model->index(row), role);
}

void TimeModelTest::testProperties()
{
    TimeModel model;
    PhotoModel photoModel;

    QSignalSpy photoModelChanged(&model, SIGNAL(photoModelChanged()));

    QVERIFY(!model.property("photoModel").value<QAbstractItemModel*>());
    model.setProperty("photoModel", QVariant::fromValue(&photoModel));
    QCOMPARE(model.property("photoModel").value<PhotoModel*>(), &photoModel);
    QCOMPARE(photoModelChanged.count(), 1);
    model.setProperty("photoModel", QVariant::fromValue((PhotoModel*)nullptr));
    QCOMPARE(photoModelChanged.count(), 2);

    QCOMPARE(model.property("date0").toDateTime(), QDateTime());
    QCOMPARE(model.property("date1").toDateTime(), QDateTime());
    QCOMPARE(model.property("count").toInt(), 0);

    QHash<int, QByteArray> expectedRoleNames {
        { TimeModel::TimeRole, "time" },
        { TimeModel::CountRole, "count" },
        { TimeModel::NormalizedCountRole, "normalizedCount" },
    };
    QCOMPARE(model.roleNames(), expectedRoleNames);
}

void TimeModelTest::testCount_data()
{
    QTest::addColumn<QStringList>("photoDates");
    QTest::addColumn<MonthCount>("expectedCounts");
    QTest::addColumn<QString>("expectedDate0");
    QTest::addColumn<QString>("expectedDate1");

    QTest::newRow("empty") <<
        QStringList {} <<
        MonthCount {} <<
        "" << "";

    QTest::newRow("invalid dates") <<
        QStringList { "", "" } <<
        MonthCount {} <<
        "" << "";

    QTest::newRow("one, first day") <<
        QStringList { "2015-07-01" } <<
        MonthCount {
            { "2015-07-01", 1 },
        } <<
        "2015-07-01" << "2015-07-01";

    QTest::newRow("one, other day") <<
        QStringList { "2015-07-05" } <<
        MonthCount {
            { "2015-07-01", 1 },
        } <<
        "2015-07-01" << "2015-07-01";

    QTest::newRow("many in one month") <<
        QStringList {
            "2015-07-05",
            "2015-07-02",
            "2015-07-02",
            "2015-07-02",
            "2015-07-05",
            "2015-07-20",
            "2015-07-29",
        } <<
        MonthCount {
            { "2015-07-01", 7 },
        } <<
        "2015-07-01" << "2015-07-01";

    QTest::newRow("with empty season") <<
        QStringList {
            "2015-04-05",
            "2015-07-20",
            "2015-07-29",
        } <<
        MonthCount {
            { "2015-04-01", 1 },
            { "2015-05-01", 0 },
            { "2015-06-01", 0 },
            { "2015-07-01", 2 },
        } <<
        "2015-04-01" << "2015-07-01";
}

void TimeModelTest::testCount()
{
    QFETCH(QStringList, photoDates);
    QFETCH(MonthCount, expectedCounts);
    QFETCH(QString, expectedDate0);
    QFETCH(QString, expectedDate1);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QList<Database::Photo> allPhotos;
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo");
    for (const QString &photoDate: photoDates) {
        photo.setTime(QDateTime::fromString(photoDate, Qt::ISODate));
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    PhotoModel photoModel;

    TimeModel model;
    QSignalSpy modelReset(&model, SIGNAL(modelReset()));
    model.setPhotoModel(QVariant::fromValue(&photoModel));
    photoModel.classBegin();
    photoModel.componentComplete();
    QCOMPARE(photoModel.rowCount(), photoDates.count());

    modelReset.wait();

    /* Extract the data from the model */
    MonthCount counts;
    for (int i = 0; i < model.rowCount(); i++) {
        QModelIndex index = model.index(i);
        QDate date = index.data(TimeModel::TimeRole).toDate();
        int count = index.data(TimeModel::CountRole).toInt();
        counts.insert(date.toString(Qt::ISODate), count);
    }

    QCOMPARE(counts, expectedCounts);
    QCOMPARE(model.date0(), QDate::fromString(expectedDate0, Qt::ISODate));
    QCOMPARE(model.date1(), QDate::fromString(expectedDate1, Qt::ISODate));
}

void TimeModelTest::testNormalizedCount_data()
{
    QTest::addColumn<QStringList>("photoDates");
    QTest::addColumn<MonthCount>("expectedCounts");

    QTest::newRow("one") <<
        QStringList { "2015-07-05" } <<
        MonthCount {
            { "2015-07-01", 100 },
        };

    QTest::newRow("many in one month") <<
        QStringList {
            "2015-07-05",
            "2015-07-02",
            "2015-07-02",
            "2015-07-02",
            "2015-07-05",
            "2015-07-20",
            "2015-07-29",
        } <<
        MonthCount {
            { "2015-07-01", 100 },
        };

    QTest::newRow("few month") <<
        QStringList {
            "2015-05-05",
            "2015-05-02",
            "2015-04-02",
            "2015-07-02",
            "2015-07-05",
            "2015-07-20",
            "2015-07-29",
        } <<
        MonthCount {
            { "2015-04-01", 25 },
            { "2015-05-01", 50 },
            { "2015-06-01", 0 },
            { "2015-07-01", 100 },
        };
}

void TimeModelTest::testNormalizedCount()
{
    QFETCH(QStringList, photoDates);
    QFETCH(MonthCount, expectedCounts);

    QScopedPointer<Database> db(Database::instance());
    DatabasePrivate *dbMock = DatabasePrivate::mocked(db.data());

    QList<Database::Photo> allPhotos;
    Database::Photo photo;
    photo.setBaseUri("/tmp");
    photo.setFileName("foo");
    for (const QString &photoDate: photoDates) {
        photo.setTime(QDateTime::fromString(photoDate, Qt::ISODate));
        allPhotos.append(photo);
    }
    dbMock->setPhotos(allPhotos);

    PhotoModel photoModel;

    TimeModel model;
    QSignalSpy modelReset(&model, SIGNAL(modelReset()));
    model.setPhotoModel(QVariant::fromValue(&photoModel));
    photoModel.classBegin();
    photoModel.componentComplete();
    QCOMPARE(photoModel.rowCount(), photoDates.count());

    modelReset.wait();

    /* Extract the data from the model */
    MonthCount counts;
    for (int i = 0; i < model.rowCount(); i++) {
        QDate date = model.get(i, "time").toDate();
        double count = model.get(i, "normalizedCount").toReal();
        counts.insert(date.toString(Qt::ISODate), int(count * 100));
    }

    QCOMPARE(counts, expectedCounts);
}

QTEST_GUILESS_MAIN(TimeModelTest)

#include "tst_time_model.moc"
