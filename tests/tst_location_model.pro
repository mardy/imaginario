TARGET = tst_location_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${SRC_DIR}/location_model.cpp \
    tst_location_model.cpp

HEADERS += \
    $${SRC_DIR}/location_model.h \
    test_utils.h
